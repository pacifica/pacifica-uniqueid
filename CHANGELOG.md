# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.5] - 2019-05-28
### Changed
- Fix #39 Added a Nice Status URL

## [0.3.4] - 2019-05-18
### Added
- Manage ORM upgrades
- ReadtheDocs supported Sphinx docs
- REST API for managing unique IDs
  - GET - Get a unique range of IDs

### Changed
